# technical

Placeholder for any free technical documents.  
All documents only use free and open formats, and commercial document formats are forbidden from the any freedocs projects.  
The formats currently recommended for use are:  
* Markdown - file extension .md text file.
* LibreOffice - file extensions .odt for Writer or .ods for Calc.

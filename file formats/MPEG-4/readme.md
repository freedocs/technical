# Main website

* https://mpeg.chiariglione.org/

# MPEG tools

* http://www.ffmpeg.org/download.html  
  
Merging video and audio can be done using ffmeg with the following example:  
**ffmpeg -i video.mp4 -i audio.m4a -c:v copy -c:a copy audio_video.mp4**

# MPEG file extensions

* .mp4 - Contains video or video + audio
* .m4a - Contains audio
* .mpg - Contains video or video + audio
* .mpeg - Contains video or video + audio
